﻿using System.Diagnostics;
using System.Media;

namespace RWFolderRenamer
{
    internal class Utils
    {
        public static string? GetProductVersion()
        {
//            var location = Assembly.GetExecutingAssembly().Location;
//            if (File.Exists(location))
//                return FileVersionInfo.GetVersionInfo(location).ProductVersion;

            var location = Environment.ProcessPath;
            if (File.Exists(location))
                return FileVersionInfo.GetVersionInfo(location).ProductVersion;

            return null;
        }

        public static void OpenUrl(string filename)
        {
            try
            {
                Process.Start("explorer.exe", filename);
            }
            catch (Exception)
            {
                SystemSounds.Beep.Play();
            }
        }

    }
}
