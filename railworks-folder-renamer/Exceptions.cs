﻿namespace RWFolderRenamer;

/// <summary>
/// Indicates that the workflow was aborted.
/// </summary>
[Obsolete]
internal class AbortException : Exception
{
    public AbortException(string message) : base(message)
    {
    }
}

/// <summary>
/// Indicates that the name of the route could not be determined..
/// </summary>
internal class NoRouteNameException : Exception
{
    public override string ToString()
    {
        return "Route name could not be determined";
    }
}
